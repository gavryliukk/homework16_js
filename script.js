const n = +prompt("Enter the sequence number");
function fibonacci (f0 = 0, f1 = 1, n){
    if (n === 0 ){
        return f0;
    }else if (n === 1 ){
        return f1;
    }else if (n > 2){
        return fibonacci(f0, f1, n - 1) + fibonacci(f0, f1, n - 2);
    }else if (n === 2){
        return f0 + f1;
    }else if (n === -1){
        return f1 - f0;
    }else if (n < -1) {
        return fibonacci(f0, f1, n + 2) - fibonacci(f0, f1, n + 1);
    }
}
console.log(fibonacci(undefined, undefined, n));


